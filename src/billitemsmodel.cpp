#include "billitemsmodel.h"

#include <QDebug>
#include <QSqlRecord>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlField>

BillItemsModel::BillItemsModel(QSqlDatabase database, QObject *parent)
    : QSqlTableModel(parent, database), m_clientId(-1), m_status(BillItem::Status::Undefined), m_billId(-1)
{
    QSqlQuery query(database);
    if (!query.exec("create table if not exists billitems "
                    "(biid integer primary key, "
                    "date varchar(20), "
                    "description varchar(100), "
                    "price real, "
                    "tax real, "
                    "status integer, "
                    "billid integer, "
                    "clientid integer)")) {
        qWarning() << "billitems table creation: " << lastError().text();
        return;
    }

    setTable("billitems");
    if (!select()) {
        qWarning() << "Failed to select table billitems: " << lastError().text();
    }
    sort(0, Qt::DescendingOrder);
}

QVariant BillItemsModel::data(const QModelIndex &index, int role) const
{
    QVariant value;

    if (index.isValid()) {
        if (role < Qt::UserRole) {
            value = QSqlTableModel::data(index, role);
        } else {
            int columnIdx = role - Qt::UserRole - 1;
            QModelIndex modelIndex = this->index(index.row(), columnIdx);
            value = QSqlTableModel::data(modelIndex, Qt::DisplayRole);
        }
    }
    return value;
}

QHash<int, QByteArray> BillItemsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    for (int i = 0; i < this->record().count(); i++) {
        roles.insert(Qt::UserRole + i + 1, record().fieldName(i).toUtf8());
    }
    return roles;
}

QSqlRecord BillItemsModel::createRecord(const BillItem &item) const
{
    QSqlRecord newRecord;
    newRecord.append(QSqlField("date", QVariant::String));
    newRecord.append(QSqlField("description", QVariant::String));
    newRecord.append(QSqlField("price", QVariant::Double));
    newRecord.append(QSqlField("tax", QVariant::Double));
    newRecord.append(QSqlField("status", QVariant::Int));
    newRecord.append(QSqlField("billid", QVariant::Int));
    newRecord.append(QSqlField("clientid", QVariant::Int));
    newRecord.setValue("date", item.date);
    newRecord.setValue("description", item.description);
    newRecord.setValue("price", item.price);
    newRecord.setValue("tax", item.tax);
    newRecord.setValue("status", BillItem::Status::NotYetInvoiced);
    newRecord.setValue("billid", -1);
    newRecord.setValue("clientid", m_clientId);
    return newRecord;
}

bool BillItemsModel::setBillId(int billId)
{
    // TODO: use setData instead?
    QSqlQuery query(database());
    if (!query.exec("update billItems set billid='" + QString::number(billId) + "' where status='" + QString::number(m_status) + "' and clientid='" + QString::number(m_clientId) + "'")) {
        qWarning() << "Failed to set billId of bill items to " << billId << ": " << lastError().text();
        return false;
    }
    return true;
}

bool BillItemsModel::setStatus(BillItem::Status status)
{
    // TODO: use setData instead?
    QSqlQuery query(database());
    if (!query.exec("update billItems set status='" + QString::number(status) + "' where status='" + QString::number(m_status) + "' and clientid='" + QString::number(m_clientId) + "'")) {
        qWarning() << "Failed to set status of bill items to " << status << ": " << lastError().text();
        return false;
    }
    return true;
}

bool BillItemsModel::setStatusByBillId(int billId, BillItem::Status status)
{
    QSqlQuery query(database());
    if (!query.exec("update billItems set status='" + QString::number(status) + "' where billid='" + QString::number(billId) + "'")) {
        qWarning() << "Failed to set status of bill items to " << status << ": " << lastError().text();
        return false;
    }
    return true;
}

/*QVariantList<BillItem> BillItemsModel::getCurrent() const
{
    QVariantList<BillItem> currentBillItems;
    for (unsigned int i=0; i<rowCount(); i++) {
        QSqlRecord rec = record(i);
        currentBillItems.append(BillItem(rec.field("date").value().toString(),
                                         rec.field("description").value().toString(),
                                         rec.field("price").value().toDouble(),
                                         rec.field("tax").value().toDouble(),
                                         rec.field("status").value().toInt(),
                                         rec.field("billid").value().toInt());
    }
    return currentBillItems;
}*/

bool BillItemsModel::add(const BillItem &item)
{
    QSqlRecord newRecord = createRecord(item);
    if (!insertRecord(-1, newRecord)) {
        qWarning() << "Failed to insert record into item database: " << lastError().text();
        return false;
    }
    select();
    return true;
}

bool BillItemsModel::remove(int index, int billItemId)
{
    beginRemoveRows(QModelIndex(), index, index);

    QSqlQuery query(database());
    if (!query.exec("delete from billItems where biid='" + QString::number(billItemId) + "'")) {
        qWarning() << "Failed to remove bill item with id " << billItemId << ": " << lastError().text();
        return false;
    }

    endRemoveRows();
    return true;
}

bool BillItemsModel::removeByClientId(int clientId)
{
    QSqlQuery query(database());
    beginResetModel();
    if (!query.exec("delete from billItems where clientid='" + QString::number(clientId) + "'")) {
        qWarning() << "Failed to remove bill items for client with id " << clientId << ": " << lastError().text();
        return false;
    }
    endResetModel();
}

bool BillItemsModel::removeByBillId(int billId)
{
    QSqlQuery query(database());
    beginResetModel();
    if (!query.exec("delete from billItems where billid='" + QString::number(billId) + "'")) {
        qWarning() << "Failed to remove bill items belonging to bill with id " << billId << ": " << lastError().text();
        return false;
    }
    endResetModel();
}

double BillItemsModel::sum() const
{
    // TODO: use data instead?
    QSqlQuery query(database());
    if (!query.exec("select price from billitems where status='" + QString::number(m_status) + "' and clientid='" + QString::number(m_clientId) + "'")) {
        qWarning() << "Failed to sum prices of bill items: " << lastError().text();
        return -1;
    }
    double total = 0.0;
    while (query.next()) {
        total += query.value(0).toDouble();
    }
    return total;
}

double BillItemsModel::tax() const
{
    // TODO: use data instead?
    QSqlQuery query(database());
    if (!query.exec("select price, tax from billitems where status='" + QString::number(m_status) + "' and clientid='" + QString::number(m_clientId) + "'")) {
        qWarning() << "Failed to calc tax of bill items: " << lastError().text();
        return -1;
    }
    double tax = 0.0;
    while (query.next()) {
        tax += query.value(0).toDouble() * query.value(1).toDouble()/100;
    }
    return tax;
}

BillItem BillItemsModel::create(const QString &date, const QString &description, double price, double tax) const
{
    return BillItem(date, description, price, tax, BillItem::Status::NotYetInvoiced, -1);
}

int BillItemsModel::clientId() const
{
    return m_clientId;
}

void BillItemsModel::setClientFilter(int clientId)
{
    m_clientId = clientId;
    updateFilter();
}

BillItem::Status BillItemsModel::status() const
{
    return m_status;
}

void BillItemsModel::setStatusFilter(BillItem::Status status)
{
    m_status = status;
    updateFilter();
}

int BillItemsModel::billId() const
{
    return m_billId;
}

void BillItemsModel::setBillIdFilter(int billId)
{
    m_billId = billId;
    updateFilter();
}


void BillItemsModel::updateFilter()
{
    QString filterCondition("");

    if (m_clientId != -1) {
        filterCondition += QString("clientid='")+QString::number(m_clientId)+QString("'");
    }

    if (m_billId != -1) {
        if (!filterCondition.isEmpty()) {
            filterCondition += " and ";
        }
        filterCondition += QString("billid='")+QString::number(m_billId)+QString("'");
    }

    if (m_status != -1) {
        if (!filterCondition.isEmpty()) {
            filterCondition += " and ";
        }
        filterCondition += QString("status='")+QString::number(m_status)+QString("'");
    }

    beginResetModel();
    setFilter(filterCondition);
    endResetModel();
    select();
    Q_EMIT filterChanged();
}

BillItemsModel::~BillItemsModel() {
    database().close();
}
