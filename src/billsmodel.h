#ifndef BILLSMODEL_H
#define BILLSMODEL_H

#include <QSqlTableModel>
#include <QSqlRecord>
//#include <QDate>

class Bill
{
    Q_GADGET
    Q_PROPERTY(QString dateInvoiced MEMBER dateInvoiced)
    Q_PROPERTY(QString dateRespite MEMBER dateRespite)
    Q_PROPERTY(QString datePayed MEMBER datePayed)
    Q_PROPERTY(double billingAmount MEMBER billingAmount)
    Q_PROPERTY(double taxIncluded MEMBER taxIncluded)
    Q_PROPERTY(double paymentReceived MEMBER paymentReceived)
    Q_PROPERTY(int id MEMBER id)
    Q_PROPERTY(int clientId MEMBER clientId)
    Q_PROPERTY(QString clientName MEMBER clientName)

public:
    explicit Bill() {};
    explicit Bill(const QString &dateInvoiced, const QString &dateRespite, const QString &datePayed, double billingAmount, double taxIncluded, double paymentReceived, int id, int clientId, QString clientName)
        : dateInvoiced(dateInvoiced), dateRespite(dateRespite), datePayed(datePayed), billingAmount(billingAmount), taxIncluded(taxIncluded), paymentReceived(paymentReceived), id(id), clientId(clientId), clientName(clientName)
    {};
    Bill(const Bill &bill)
        : dateInvoiced(bill.dateInvoiced), dateRespite(bill.dateRespite), datePayed(bill.datePayed), billingAmount(bill.billingAmount), taxIncluded(bill.taxIncluded), paymentReceived(bill.paymentReceived), id(bill.id), clientId(bill.clientId), clientName(bill.clientName)
    {};

    QString dateInvoiced;
    QString dateRespite;
    QString datePayed;
    double billingAmount;
    double taxIncluded;
    double paymentReceived;
    int id;
    int clientId;
    QString clientName;
};

class BillsModel : public QSqlTableModel
{
    Q_OBJECT

public:
    explicit BillsModel(QSqlDatabase database, QObject *parent = 0);
    explicit BillsModel(const BillsModel & billsModel) {}
    explicit BillsModel() {}
    virtual ~BillsModel();

    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    bool setCurrentBill(int index);
    bool add(int clientId, const QString &clientName, double billingAmount, double taxIncluded);
    bool removeByClientId(int clientId);
    bool remove(int index);

    Bill currentBill;

Q_SIGNALS:
    void databaseChanged();
    void currentBillChanged();

private:
    QSqlRecord createRecord(int clientId, const QString &clientName, double billingAmount, double taxIncluded) const;
    bool setCurrentBill(const QSqlRecord &record);
};

#endif
