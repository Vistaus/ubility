cmake_minimum_required(VERSION 3.0.0)
project(ubility C CXX)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

find_package(Qt5Core)
find_package(Qt5Qml)
find_package(Qt5Quick)

# Automatically create moc files
set(CMAKE_AUTOMOC ON)

# Components PATH
execute_process(
    COMMAND dpkg-architecture -qDEB_HOST_MULTIARCH
    OUTPUT_VARIABLE ARCH_TRIPLET
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

set(PROJECT_NAME "ubility")
set(FULL_PROJECT_NAME "ubility.hummlbach")
set(CMAKE_INSTALL_PREFIX /)
set(DATA_DIR /)
set(DESKTOP_FILE_NAME ${PROJECT_NAME}.desktop)

# This command figures out the target architecture for use in the manifest file
execute_process(
    COMMAND dpkg-architecture -qDEB_HOST_ARCH
    OUTPUT_VARIABLE CLICK_ARCH
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

configure_file(manifest.json.in ${CMAKE_CURRENT_BINARY_DIR}/manifest.json)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/manifest.json DESTINATION ${CMAKE_INSTALL_PREFIX})
install(FILES ${PROJECT_NAME}.apparmor DESTINATION ${DATA_DIR})
install(FILES content-hub.json DESTINATION ${DATA_DIR})
install(DIRECTORY qml/ui DESTINATION ${DATA_DIR})
install(DIRECTORY assets DESTINATION ${DATA_DIR})

if("${ARCH_TRIPLET}" STREQUAL "x86_64-linux-gnu")
    install(FILES qml/share/urihandler/FileShareComponent.qml DESTINATION ${DATA_DIR}ui/components)
else()
    install(FILES qml/share/contenthub/FileShareComponent.qml DESTINATION ${DATA_DIR}ui/components)
endif()

add_executable(${PROJECT_NAME} src/main.cpp src/clientsmodel.cpp src/billitemsmodel.cpp src/billsmodel.cpp src/ubilitydatabase.cpp src/biller.h src/productsmodel.cpp)
qt5_use_modules(${PROJECT_NAME} Gui Qml Quick QuickControls2 Sql PrintSupport WebKitWidgets)
install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX})

if(NOT SKIP_WEBKIT)
    set(QT_IMPORTS_DIR "lib/${ARCH_TRIPLET}")
    file(GLOB QTWEBKIT_LIBS "${CMAKE_CURRENT_SOURCE_DIR}/build/${ARCH_TRIPLET}/qtwebkit/install/lib/${ARCH_TRIPLET}/lib*.so*")
    install(FILES ${QTWEBKIT_LIBS} DESTINATION ${QT_IMPORTS_DIR})
endif()

# Translations
file(GLOB_RECURSE I18N_SRC_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/po qml/*.qml qml/*.js)
list(APPEND I18N_SRC_FILES ${DESKTOP_FILE_NAME}.in.h)

find_program(INTLTOOL_MERGE intltool-merge)
if(NOT INTLTOOL_MERGE)
    message(FATAL_ERROR "Could not find intltool-merge, please install the intltool package")
endif()
find_program(INTLTOOL_EXTRACT intltool-extract)
if(NOT INTLTOOL_EXTRACT)
    message(FATAL_ERROR "Could not find intltool-extract, please install the intltool package")
endif()

add_custom_target(${DESKTOP_FILE_NAME} ALL
    COMMENT "Merging translations into ${DESKTOP_FILE_NAME}..."
    COMMAND LC_ALL=C ${INTLTOOL_MERGE} -d -u ${CMAKE_SOURCE_DIR}/po ${CMAKE_SOURCE_DIR}/${DESKTOP_FILE_NAME}.in ${DESKTOP_FILE_NAME}
    COMMAND sed -i 's/${PROJECT_NAME}-//g' ${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE_NAME}
)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE_NAME} DESTINATION ${DATA_DIR})

add_subdirectory(po)

# Make source files visible in qtcreator
file(GLOB_RECURSE PROJECT_SRC_FILES
    RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
    qml/ui/pages/*.qml
    qml/ui/components/*.qml
    qml/ui/*.qml
    *.json
    *.json.in
    *.apparmor
    *.desktop.in
)

add_custom_target(${PROJECT_NAME}_FILES ALL SOURCES ${PROJECT_SRC_FILES})
