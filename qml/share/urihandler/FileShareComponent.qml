import QtQuick 2.4

Item {
    signal openUrlExternally(var filename);
    signal openContentPeerPickerPage(var filename);

    function share(filename) {
        openUrlExternally(filename);
    }
}
