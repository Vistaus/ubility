import QtQuick 2.4

Item {
    signal openContentPeerPickerPage(var filename);
    signal openUrlExternally(var filename);

    function share(filename) {
        openContentPeerPickerPage(filename)
    }
}
