/*
 * Copyright (C) 2019  Johannes Renkl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "../components"


UITK.Page {
    id: settingsPage
    anchors.fill: parent

    property var biller: null
    property var products: null

    signal openAbout()

    header: UITK.PageHeader {
        id: header
        title: i18n.tr('Settings')

        trailingActionBar.actions: [
            UITK.Action {
                id: goSettings
                objectName: "openAbout"
                text: i18n.tr("About")
                iconName: "info"
                onTriggered: {
                    settingsPage.openAbout()
                }
            }
        ]

        extension: UITK.Sections {
            id: menuSections
            anchors.verticalCenter: parent.verticalCenter 
            anchors.horizontalCenter: parent.horizontalCenter 

            actions: [
                UITK.Action {
                    text: i18n.tr("Biller")
                    onTriggered: {
                        billerSettingsForm.visible = true;
                        productListView.visible = false;
                    }
                },
                UITK.Action {
                    text: i18n.tr("Products")
                    onTriggered: {
                        productListView.visible = true;
                        billerSettingsForm.visible = false;
                    }
                }
            ]
        }
    }

    BillerSettingsForm {
        id: billerSettingsForm

        biller: settingsPage.biller

        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: units.gu(2)
        }
    }

    ProductListView {
        id: productListView

        products: settingsPage.products

        anchors {
            fill: parent
            topMargin: header.height
        }
    }
}
