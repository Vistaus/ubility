import QtQuick 2.7
import Ubuntu.Components.Themes 1.3
import QtQuick.Controls 2.2

Rectangle {
    id: dividerRect
    width: parent.width      
    height: 1
    color: theme.palette.normal.base
}
