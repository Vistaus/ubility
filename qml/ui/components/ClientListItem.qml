import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

UITK.ListItem {
    width: parent.width

    signal remove()
    signal edit()

    property var firstname: null
    property var lastname: null
    property bool showRemove: true

    leadingActions: UITK.ListItemActions {
        actions: [
            UITK.Action {
                iconName: "delete"
                text: i18n.tr("Remove")
                visible: showRemove
                onTriggered: remove()
            }
        ]
    }
    trailingActions: UITK.ListItemActions {
        actions: [
            UITK.Action {
                iconName: "edit"
                text: i18n.tr("Edit")
                onTriggered: edit()
            }
        ]
    }


    ColumnLayout {
        Layout.alignment: Qt.AlignVCenter
        Label {
            elide: Text.ElideRight
            text: firstname + " " + lastname
            Layout.leftMargin: units.gu(2)
            Layout.topMargin: units.gu(1)
        }
    }
}
