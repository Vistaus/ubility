import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls.Suru 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

UITK.ListItem {
    width: parent.width

    signal remove()
    signal edit()

    property var product: null

    leadingActions: UITK.ListItemActions {
        actions: [
            UITK.Action {
                iconName: "delete"
                text: i18n.tr("Remove")
                onTriggered: remove()
            }
        ]
    }

    //trailingActions: UITK.ListItemActions {
    //    actions: [
    //        UITK.Action {
    //            iconName: "edit"
    //            text: i18n.tr("Edit")
    //            onTriggered: edit()
    //        }
    //    ]
    //}

    ColumnLayout {
        spacing: units.gu(1)
        Layout.fillWidth: true
        anchors {
            left: parent.left
            right: parent.right
            margins: units.gu(2)
        }

        RowLayout {
            spacing: units.gu(2)
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignVCenter
            Layout.topMargin: units.gu(1)

            Label {
                elide: Text.ElideRight
                text: product.title
                Layout.alignment: Qt.AlignLeft
                Layout.preferredWidth: parent.width/2
            }

            Label {
                elide: Text.ElideRight
                text: product.price
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft
            }

            Label {
                elide: Text.ElideRight
                text: i18n.tr("%1%").arg(product.tax)
                Layout.alignment: Qt.AlignRight
            }
        }

        Label {
            elide: Text.ElideRight
            text: product.description
            Layout.alignment: Qt.AlignLeft
        }
    }
}
