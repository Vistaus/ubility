import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    id: formTextInput
    property string fieldName: ""
    property string placeHolderText: ""
    readonly property string text: textInputField.text

    function insert(text) {
        textInputField.insert(0, text)
    }

    ColumnLayout {
        id: columnLayout
        Layout.alignment: Qt.AlignTop
        anchors.fill: parent

        UITK.TextField {
            id: textInputField
            placeholderText: formTextInput.placeHolderText
            Layout.preferredWidth: formTextInput.width
        }

        Label {
            id: fieldNameLabel
            elide: Text.ElideRight
            //font.pixelSize: 15
            font.italic: true
            text: fieldName
        }
    }
}
